//
//  MTSelectPlayerView.m
//  lessen_view
//
//  Created by 葛 智紀 on 2015/06/22.
//  Copyright (c) 2015年 葛 智紀. All rights reserved.
//

#import "MTSelectPlayerView.h"

@interface MTSelectPlayerView()
/** プレーヤー1名 */
@property UILabel *firstPlayer;
/** プレーヤー1選択状態 */
@property UIButton *firstCheck;

/** プレーヤー2名 */
@property UILabel *secondPlayer;
/** プレーヤー2 選択状態 */
@property UIButton *secondCheck;

@end

@implementation MTSelectPlayerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor redColor];
        [self addOthersView];
    }
    
    return self;
}

- (void)addOthersView{
    
    self.firstPlayer = [[UILabel alloc]initWithFrame:CGRectZero];
    self.firstPlayer.backgroundColor = [UIColor greenColor];
    self.firstPlayer.text = @"firstPlayer";
    
    self.firstCheck = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.firstCheck.backgroundColor = [UIColor yellowColor];
    
    [self addSubview:self.firstPlayer];
    [self addSubview:self.firstCheck];
    
    self.secondPlayer = [[UILabel alloc]initWithFrame:CGRectZero];
    self.secondPlayer.backgroundColor = [UIColor greenColor];
    self.secondPlayer.text = @"secondPlayer";
    
    self.secondCheck = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.secondCheck.backgroundColor = [UIColor yellowColor];
    
    [self addSubview:self.secondPlayer];
    [self addSubview:self.secondCheck];
}

-(void)updateLayout
{
    self.firstPlayer.frame = CGRectMake(10 , 10 , self.frame.size.width/1.5, self.frame.size.height/3);
    self.firstCheck.frame = CGRectMake(self.frame.size.width-10-self.frame.size.height/3, 10,self.frame.size.height/3 ,self.frame.size.height/3 );
    
    self.secondPlayer.frame = CGRectMake(10, self.firstPlayer.frame.size.height+10*2, self.frame.size.width/1.5, self.frame.size.height/3);
    self.secondCheck.frame = CGRectMake(self.frame.size.width-10-self.frame.size.height/3, self.firstPlayer.frame.size.height+10*2,self.frame.size.height/3 ,self.frame.size.height/3 );
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
