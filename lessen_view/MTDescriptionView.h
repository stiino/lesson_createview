//
//  MTDescriptionView.h
//  lessen_view
//
//  Created by 葛 智紀 on 2015/06/22.
//  Copyright (c) 2015年 葛 智紀. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * 説明文のビュー
 * TextView *Description :説明文
 */
@interface MTDescriptionView : UIView

- (void)updateLayout;

@end
