//
//  main.m
//  lessen_view
//
//  Created by 葛 智紀 on 2015/06/22.
//  Copyright (c) 2015年 葛 智紀. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
