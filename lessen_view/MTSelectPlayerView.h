//
//  MTSelectPlayerView.h
//  lessen_view
//
//  Created by 葛 智紀 on 2015/06/22.
//  Copyright (c) 2015年 葛 智紀. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *プレーヤーを選択するビュー
 */
@interface MTSelectPlayerView : UIView

-(void)updateLayout;

@end
