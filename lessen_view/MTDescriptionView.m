//
//  MTDescriptionView.m
//  lessen_view
//
//  Created by 葛 智紀 on 2015/06/22.
//  Copyright (c) 2015年 葛 智紀. All rights reserved.
//

#import "MTDescriptionView.h"

@interface MTDescriptionView()

/** 説明文を表示するびゅー */
@property UITextView *descriptionText;

/** なんかのパーツ */
@property UIView *partsView;

@end

@implementation MTDescriptionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor greenColor];
        [self addOthersView];
    }
    
    return self;
    
}

- (void)addOthersView {
    
    self.descriptionText = [[UITextView alloc]initWithFrame: CGRectZero];
    self.descriptionText.backgroundColor = [UIColor redColor];
    self.descriptionText.text = @"ここに説明かく";
    self.descriptionText.editable = NO;
    
    [self addSubview:self.descriptionText];
    
    self.partsView = [[UIView alloc]initWithFrame:CGRectZero];
    self.partsView.backgroundColor = [UIColor orangeColor];
    
    [self addSubview:self.partsView];
    
}

-(void)updateLayout
{
    self.descriptionText.frame = CGRectMake(10, 10, self.frame.size.width-20, self.frame.size.height/1.5);
    
    self.partsView.frame = CGRectMake(self.frame.size.width/4, 20+self.frame.size.height/1.5, self.frame.size.width/2, self.frame.size.height/5);
}

@end
