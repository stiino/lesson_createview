//
//  ViewController.m
//  lessen_view
//
//  Created by 葛 智紀 on 2015/06/22.
//  Copyright (c) 2015年 葛 智紀. All rights reserved.
//

#import "ViewController.h"
#import "MTDescriptionView.h"
#import "MTSelectPlayerView.h"

@interface ViewController ()
/** 横にした時にスクロールできるようにスクロールビューを配置 */
@property UIScrollView *scrollView;

/** 説明文のビュー */
@property MTDescriptionView *descriptionView;
/** プレーヤー選択するビュー */
@property MTSelectPlayerView *selectPlayerView;

/** スタートボタン */
@property UIButton *startButton;

/** 画面比率 */
@property (nonatomic,readonly) NSNumber *screenProportion;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //スクロールビューを生成
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectZero];
    
    [self.view addSubview:self.scrollView];
    
    //画面の幅の広い広いところを取得

    
    //説明文のビューを実装
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view, typically from a nib.
    self.descriptionView = [[MTDescriptionView alloc]initWithFrame:CGRectZero];
    [self.scrollView addSubview:self.descriptionView];
    
    //プレーヤー選択のビューを実装
    self.selectPlayerView = [[MTSelectPlayerView alloc]initWithFrame:CGRectZero];
    [self.scrollView addSubview:self.selectPlayerView];
    
    //スタートボタン
    self.startButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.startButton.backgroundColor = [UIColor blueColor];
    [self.startButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.startButton setTitle:@"すたーと" forState:UIControlStateNormal];
    
    [self.scrollView addSubview:self.startButton];
    
    [self updateLayout:self.view.frame.size];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSNumber*)screenProportion
{
    NSNumber *proportionNum;
    if (self.view.frame.size.width <= self.view.frame.size.height) {
        proportionNum = [[NSNumber alloc]initWithFloat:self.view.frame.size.height/self.view.frame.size.width];
    }else{
        proportionNum = [[NSNumber alloc]initWithFloat:self.view.frame.size.width/self.view.frame.size.height];
    }
    return  proportionNum;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [self updateLayout:size];
}

- (void)updateLayout:(CGSize)size
{
    self.scrollView.frame = CGRectMake(0,0,size.width, size.height);
    self.scrollView.contentSize = CGSizeMake(size.width, size.width*self.screenProportion.floatValue);
    
    self.descriptionView.frame = CGRectMake(50 , 50 , size.width-100, (size.width*self.screenProportion.floatValue)/3);
    [self.descriptionView updateLayout];
    
    self.selectPlayerView.frame = CGRectMake(50, 50*2+self.descriptionView.frame.size.height, size.width-100, (size.width - 100) / 2);
    [self.selectPlayerView updateLayout];
    
    self.startButton.frame = CGRectMake(size.width/3, (size.width*self.screenProportion.floatValue)-50-(size.width/3)/2,size.width/3 , (size.width/3)/2);
}

@end
